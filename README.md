# Awesome Tools

A curated list of awesome tools for various purposes.

## Table of Contents

<!--toc:start-->
- [Awesome Tools](#awesome-tools)
  - [Table of Contents](#table-of-contents)
  - [Decentralized Technology](#decentralized-technology)
    - [Alternative-Internet](#alternative-internet)
  - [Messaging](#messaging)
    - [Salty.im](#saltyim)
  - [File Sharing](#file-sharing)
    - [Croc](#croc)
  - [Contributing](#contributing)
  - [License](#license)
<!--toc:end-->

## Decentralized Technology

### Alternative-Internet

- [https://github.com/prologic/alternative-internet](https://github.com/prologic/alternative-internet)
- [https://github.com/redecentralize/alternative-internet](https://github.com/redecentralize/alternative-internet)

## Messaging

### Salty.im

- [https://git.mills.io/saltyim/saltyim](https://git.mills.io/saltyim/saltyim)

Secure, easy, self-hosted messaging

## File Sharing

### Croc

- [https://github.com/schollz/croc](https://github.com/schollz/croc)

## Contributing

Contributions are welcome! If you know of a tool that should be added to this list, please follow the [contributing guidelines](CONTRIBUTING.md).

## License

This project is licensed under the [MIT License](LICENSE).
